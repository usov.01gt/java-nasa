import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class NASAConnection {

    public static final String apiKey = "IdSebOvSoDWlDKXCVUbv5vnl6AmSvBj2qWFAyjz9";
    public static final String requestData = "?api_key="+apiKey;
    public static final URL url;

    static {
        try {
            url = new URL("https://api.nasa.gov/planetary/apod"+requestData);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @return
     * @throws IOException
     *
     * подключение к сайту наса и получение данных в формате json
     */
    public static final String getConnection () throws IOException {

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoOutput(true);

        String responseData = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                connection.getInputStream()))) {
            String line;
            if ((line = br.readLine()) != null) {
                responseData = line;
            }
        }

        return  responseData;
    }

}
