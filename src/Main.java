import java.io.File;
import java.io.IOException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static final String PathToSave = "images/";

    /**
     *
     * @param url
     * @throws Exception
     *
     * метод сохранения файла
     */
    public static void downloadFile(String url) throws Exception {
        String[] UrlArray = url.split("/");
        String FileName = UrlArray[UrlArray.length - 1];

        File File = new File(PathToSave + FileName);

        if (!File.isFile()) {
            try (InputStream in = new URL(url).openStream()) {
                System.out.println(File.toPath());
                Files.copy(in, Paths.get(File.getPath()));
            }
            System.out.println("File copy done");
        } else {
            System.out.println("File is already existing");
        }


    }


    public static String getUrlImage() {

        String NasaImageUrl;
        try {
            Object NasaJsonObject = new JSONObject(NASAConnection.getConnection());
            NasaImageUrl = ((JSONObject) NasaJsonObject).getString("hdurl");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return NasaImageUrl;
    }

    /**
     * @param args
     *
     * главный метод, вызывает метод полученияя json данных с сайта наса, чтения url строки файла и последующее скачивание
     */
    public static void main(String[] args) throws IOException {

        try {
            String NasaImageUrl = getUrlImage();
            downloadFile(NasaImageUrl);
            new MyTelegramBot(NasaImageUrl);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}