import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class MyTelegramBot extends TelegramLongPollingBot
{

    private static final String BOT_TOKEN = "6085624230:AAGNvOBANK08a0HAGQWt5l8I3vhFoJIDAsA";
    private static final String BOT_USERNAME = "ThisIs_Us007_Nasa_Bot";

    public static long chat_id;


    public MyTelegramBot(String nasaImageUrl) throws TelegramApiException {
        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
        botsApi.registerBot(this);
    }


    /**
     * @return
     */
    @Override
    public String getBotUsername() {
        return BOT_USERNAME;
    }

    /**
     * @return
     */
    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }

    /**
     * @param update
     */
    @Override
    public void onUpdateReceived(Update update) {

        System.out.println(update.getMessage().getText());

        if (update.hasMessage()) {
            chat_id = update.getMessage().getChatId();
            switch (update.getMessage().getText()) {
                case "/help":
                    sendMessage("Привет, я бот NASA! Я высылаю ссылки на картинки по запросу. " +
                            "Напоминаю, что картинки на сайте NASA обновляются раз в сутки");
                    break;
                case "/give":
                    sendMessage("Привет? Вероника, чика-чика!!!");
                    sendMessage(Main.getUrlImage());
                    break;
                default:
                    sendMessage("Я не понимаю :(");
            }
        }

    }


    private void sendMessage(String messageText) {
        System.out.println(chat_id);
        SendMessage message = new SendMessage();
        message.setChatId(chat_id);
        message.setText(messageText);
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
